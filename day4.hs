module Main where

import Data.List
import Data.List.Split
import Data.Functor

type Board = [[Int]]

main = do input <- getContents
          let (numstr:_:boardstrs) = lines input
              boards = map (constructBoard . map (map read . words)) (splitOn [""] boardstrs)
              nums   = map read $ splitOn "," numstr :: [Int]

          let result      = bingo boards nums
              resultScore = flip finalScore nums <$> result
          print $ (\(x,_,_) -> x) <$> result
          print resultScore

          let lastResult      = bingoTillNothing boards nums
              lastResultScore = flip finalScore nums <$> lastResult

          print $ (\(x,_,_) -> x) <$> lastResult
          print lastResultScore
          putStrLn "trolled"

bingoTillNothing :: [Board] -> [Int] -> Maybe ([Board], [Board], [Int])
bingoTillNothing bs ns =
    let result = bingo bs ns
        next   = result >>= (\(_,x,y) -> bingo x y)
    in case next of
         Nothing -> result
         Just _  -> result >>= (\(_,x,y) -> bingoTillNothing x y)

uncheckedNums :: Board -> [Int]
uncheckedNums = foldl union []

finalScore :: ([Board], [Board], [Int]) -> [Int] -> [Int]
finalScore (winners, bs, nsLeft) ns =
    let finalNum = last $ head $ splitOn nsLeft ns
     in map (`score` finalNum) winners

score :: Board -> Int -> Int
score b n = n * sum (uncheckedNums b)

bingo :: [Board] -> [Int] -> Maybe ([Board], [Board], [Int])
bingo bs (n:ns) =
    let played = map (callNumber n) bs
     in if any hasWon played
           then Just ( filter hasWon played
                     , filter (not . hasWon) played
                     , ns
                     )
           else bingo played ns
bingo _ [] = Nothing

callNumber :: Int -> Board -> Board
callNumber n = map (filter (/= n))

hasWon :: Board -> Bool
hasWon = any null

constructBoard :: [[Int]] -> Board
constructBoard ns =
    let diagonals = [ getDiagonal head tail ns
                    , getDiagonal last init ns
                    ]
        cols      = getCols ns
        rows      = ns
    in cols ++ rows
   where
       getDiagonal _ _ [] = []
       getDiagonal f g (l:ls) = f l : getDiagonal f g (map g ls)
       getCols [] = repeat []
       getCols (l:ls) = zipWith (++) (map (:[]) l) (getCols ls)


module Main where

import Data.List.Split
import Data.Array.Unboxed

type Point = (Int, Int)
type Line  = (Point, Point)

main = do
    input <- getContents
    let parsedLines    = map parseLine $ lines input
        linePoints     = [genHLine, genVLine] >>= (parsedLines >>=)
        intersections  = findPointIntersections linePoints
        linePoints2    = [genHLine, genVLine, genDLine] >>= (parsedLines >>=)
        intersections2 = findPointIntersections linePoints2
    print $ length intersections
    print $ length intersections2
    putStrLn "trolled"

genDLine :: Line -> [Point]
genDLine (p@(px,py), q@(qx, qy))
  | p == q    = [p]
  | px == qx  = []
  | py == qy  = []
  | otherwise = zip (range px qx) (range py qy)
    where
        range a b
          | a <= b = [a..b]
          | a > b  = reverse [b..a]

findPointIntersections :: [Point] -> [(Point, Int)]
findPointIntersections = filter ((>1) . snd) . listify . countUp . createMatrix
    where
        createMatrix points =
            let width  = maximum $ map fst points
                height = maximum $ map snd points
                offx   = minimum $ map fst points
                offy   = minimum $ map snd points
                matrix = listArray 
                  ((offx, offy), (width, height))
                  (repeat 0) 
                  :: UArray Point Int
             in (matrix, points)
        countUp (matrix, points) = accum (+) matrix $ zip points (repeat 1)
        listify = assocs

genHLine :: Line -> [Point]
genHLine ((px,y), (qx,qy))
  | y /= qy   = []
  | px == qx  = []
  | otherwise =
      let sx = min px qx
          ex = max px qx
       in zip [sx..ex] (repeat y)

genVLine :: Line -> [Point]
genVLine ((x, py), (qx, qy))
  | x /= qx   = []
  | py == qy  = []
  | otherwise =
      let sy = min py qy
          ey = max py qy
       in zip (repeat x) [sy..ey]

parseLine :: String -> Line
parseLine = tuplify . map (tuplify . map read . splitOn ",") . splitOn " -> "
    where tuplify [x,y] = (x,y)
          tuplify _ = error "incorrect use of tuplify"

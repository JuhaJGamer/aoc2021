module Main where

data Command = Forward Int | Down Int | Up Int
    deriving (Show, Eq)

type State = (Int, Int)
type State2 = (Int, Int, Int)

main = do input <- getContents
          let commands = map parseCommand $ lines input
              state    = executeCommands (0,0) commands
              state2   = executeCommands2 (0,0,0) commands
          print state
          print $ uncurry (*) state
          print state2
          print $ (\(d,x,_) -> d*x) state2
          putStrLn "trolled"

parseCommand :: String -> Command
parseCommand s = let (cmd:n:cs) = words s
                     number     = read n
                 in case cmd of
                 "forward" -> Forward number
                 "down"    -> Down number
                 "up"      -> Up number


executeCommands :: State -> [Command] -> State
executeCommands state [] = state
executeCommands (d,x) (cmd:cmds) =
    let newState = case cmd of
            Forward n -> (d, x+n)
            Down    n -> (d+n, x)
            Up      n -> (d-n, x)
    in executeCommands newState cmds

executeCommands2 :: State2 -> [Command] -> State2
executeCommands2 state [] = state
executeCommands2 (d,x,a) (cmd:cmds) =
    let newState = case cmd of
            Forward n -> (d+n*a, x+n, a)
            Down    n -> (d, x, a+n)
            Up      n -> (d, x, a-n)
    in executeCommands2 newState cmds



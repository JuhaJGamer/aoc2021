from enum import Enum
from sys import stdin
from typing import List

Measurement = Enum('Measurement', 'Increased Decreased Equal')

def measure(a: int, b: int) -> Measurement:
    if a > b:
        return Measurement.Increased
    elif a < b:
        return Measurement.Decreased
    else:
        return Measurement.Equal


def measure_depths(l: List[int]) -> List[Measurement]:
    return list(map(lambda t: measure(*t), zip(l[1:], l)))


if __name__ == "__main__":
    depths = list(map(int, stdin.readlines()))
    measurements = measure_depths(depths)
    print(len(list(filter(lambda m: m == Measurement.Increased, measurements))))

    windows = zip(depths, depths[1:], depths[2:])
    winsums = list(map(lambda t: t[0]+t[1]+t[2], windows))
    print(winsums[:10])
    winmeasurements = measure_depths(winsums)
    print(len(list(filter(lambda m: m == Measurement.Increased, winmeasurements))))

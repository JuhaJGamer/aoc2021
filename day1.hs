module Main where

data Measurement = Inc | Dec | Equal
    deriving Eq

main = do input <- getContents
          let depths = readDepths input
              win    = window depths
              ms     = measureDepths depths
              wms    = measureDepths win
          print $ length $ filter (== Inc) ms
          print $ length $ filter (== Inc) wms
          putStrLn "trolled"


readDepths :: String -> [Int]
readDepths s = map read $ lines s

measure :: Ord a => a -> a -> Measurement
measure a b
    | a < b     = Dec
    | a > b     = Inc
    | otherwise = Equal

measureDepths :: [Int] -> [Measurement]
measureDepths l = zipWith measure (drop 1 l) l

window :: [Int] -> [Int]
window l = zipWith (\x (y,z) -> x+y+z) l $ zip (drop 1 l) (drop 2 l)


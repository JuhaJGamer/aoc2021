module Main where

import Data.Maybe
import Data.List

main = do 
    code <- lines <$> getContents
    let illegalChars = catMaybes $ map findIllegal code
        illegalScore = corScore illegalChars
        completions  = catMaybes $ map complete code
        compScores   = map compScore completions
    print illegalScore
    print $ median compScores
    putStrLn "trolled"

median :: (Ord a) => [a] -> a
median xs = 
    let i = length xs `div` 2
        s = sort xs
     in s !! i

match :: Char -> Char
match '(' = ')'
match '[' = ']'
match '{' = '}'
match '<' = '>'
match _   = undefined

matches :: Char -> Char -> Bool
matches c = (== match c)

complete :: String -> Maybe String
complete = complete' []
    where 
        complete' ss [] = Just $ map match ss
        complete' [] (c:cs) = complete' [c] cs
        complete' (s:ss) (c:cs)
          | c `elem` "([{<" = complete' (c:s:ss) cs
          | s `matches` c   = complete' ss cs
          | otherwise       = Nothing

compScore :: String -> Int
compScore = foldl compScore' 0
    where
        compScore' z c = 
            let cScore = 
                    case c of
                        ')' -> 1
                        ']' -> 2
                        '}' -> 3
                        '>' -> 4
                        _   -> undefined
              in z*5 + cScore

findIllegal :: String -> Maybe Char
findIllegal = findIllegal' []
    where
        findIllegal' [] (c:cs) = findIllegal' [c] cs
        findIllegal' _ [] = Nothing
        findIllegal' (s:ss) (c:cs)
            | c `elem` "([{<" = findIllegal' (c:s:ss) cs
            | s `matches` c = findIllegal' ss cs
            | otherwise     = Just c

corScore :: [Char] -> Int
corScore [] = 0
corScore (c:cs) =
    corScore cs
    + case  c of
        ')' -> 3
        ']' -> 57
        '}' -> 1197
        '>' -> 25137
        _   -> undefined


module Main where

import Data.List
import Debug.Trace

main = do input <- getContents
          let ratefactors = rateFactors $ lines input
              gamma       = [ if r < 0 then 0 else 1 | r <- ratefactors ] :: [Int]
              epsilon     = [ if r > 0 then 0 else 1 | r <- ratefactors ] :: [Int]
              gamma_r     = binaryToInt gamma
              epsilon_r   = binaryToInt epsilon
              o2          = findValue o2Criteria $ lines input
              co2         = findValue co2Criteria $ lines input
              o2_r        = binaryToInt [ read [c] | c <- o2 ]
              co2_r       = binaryToInt [ read [c] | c <- co2 ]
          print ratefactors
          print (gamma_r, epsilon_r)
          print (gamma_r * epsilon_r)
          print (o2, co2)
          print (o2_r, co2_r)
          print (o2_r * co2_r)
          putStrLn "trolled"


rateFactors :: [String] -> [Int]
rateFactors []     = repeat 0
rateFactors (s:ss) =
    let factors = map parse s
    in zipWith (+) factors $ rateFactors ss
    where
        parse :: Char -> Int
        parse '0' = -1
        parse '1' =  1
        parse _   =  0


binaryToInt :: [Int] -> Int
binaryToInt l = sum [ 2^n * r | (r,n) <- zip (reverse l) [0..] ]

findValue :: (Int -> Char) -> [String] -> String
findValue bitCriteria ss = filter ss
    where
        filter [s] = s
        filter ss =
            let r       = head $ rateFactors ss
                poschar = bitCriteria r
             in poschar : filter [ tail s | s <- ss, head s == poschar ]

co2Criteria :: Int -> Char
co2Criteria r
    | r < 0     = '1'
    | otherwise = '0'

o2Criteria :: Int -> Char
o2Criteria r
  | r < 0     = '0'
  | otherwise = '1'
